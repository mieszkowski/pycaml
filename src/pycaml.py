from PARSE import PARSE
from GENERATE import GENERATE

from sys import argv

def main():
	arg = argv[1]
	p = PARSE(arg)
	g = GENERATE(p.ast, p.file)
	g.visit(g.ast)
	g.write_to_file()




if __name__ == '__main__':
	main()
