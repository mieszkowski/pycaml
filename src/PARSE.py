from sys import argv
from os.path import isfile, isdir
from ast import parse

class PARSE:
	def __init__(self, arg):
		if (isfile(arg)):
			self.file = arg
			self.load_file(arg)
		elif (isdir(arg)):
			self.parse_directory(arg)

	def load_file(self, arg):
		with open(arg, 'r') as f:
			self.data = f.read()
		self.ast = parse(self.data, 
							filename=arg, 
							mode="exec")

	def parse_directry(self, arg):
		pass#walk() aver here
