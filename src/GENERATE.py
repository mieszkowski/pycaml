from ast import NodeVisitor, dump

import _ast

class GENERATE(NodeVisitor):
	def __init__(self, ast, fname):
		self.ast = ast
		self.file = fname + '.ml'
		self.data = ''
		self.classflag = False
		self.methodflag = False
		print dump(ast)


	def write_to_file(self):
		with open(self.file, 'w') as f:
			print self.data
			f.write(self.data)

	def class_args(self, body):
		s = ""
		for n in body:
			if (hasattr(n, 'args')):
				for a in n.args.args:
					if (a.id == "self"):
						continue
					elif (n.name == "__init__"):
						s += a.id
					else:
						pass
		return s

	def method_body(self, body):
		if isinstance(body[0], _ast.Pass):
			pass
			#self.data += "()"
		else:
			pass	
		for b in body:
			super(GENERATE, self).generic_visit(b)

	def screen_recursive(self, name, body):
		return False
#if name in body return true else return None

	#############################################################

	def visit_Pass(self, node):
		print "PASS"
		self.data += '()'

	def visit_keyword(self, node):
		print "KEYWORD"
	#keyword = (identifier arg, expr value)	

	def visit_Import(self, node):
		print "IMPORT"
	#Import(alias* names)
		for n in node.names:
			self.data += "open " + n.name + ';\n'
	#	super(GENERATE, self).generic_visit(node) 

	def visit_ImportFrom(self, node):
		print "IMPORTFROM"
	#ImportFrom(identifier? module, alias* names, int? level)
		super(GENERATE, self).generic_visit(node) 
	
	def visit_Add(self,node):
		print "ADD"
		self.data += " + "
	def visit_Sub(self,node):
		print "SUB"
		self.data += " - "
	def visit_Mult(self,node):
		print "MULT"
		self.data += " * "
	def visit_Div(self,node):
		print "DIV"
		self.data += " / "
	def visit_Mod(self, node):
		print ""
	def visit_Pow(self, node):
		print ""#TODO
	def visit_Mod(self, node):
		print "MOD"
		self.data += ' mod '


	def visit_Str(self, node):
		print "STR"
	#Str(string s)
		self.data +=  '"' + node.s + '"'

	def visit_Num(self, node):
		print "NUM"
	#Num(object n)
		self.data += ' ' + str(node.n) + ' '

	def visit_Name(self, node):
		print "NAME"
	#Name(identifier id, expr_context ctx)
		self.data += node.id + " "

	def visit_Print(self, node):
		print "PRINT"
	#Print(expr? dest, expr* values, bool nl)
		self.data += 'Printf.printf ' 
		super(GENERATE, self).generic_visit(node) 
		self.data += ";;\n"


	def visit_Call(self, node):
		print "CALL"
	# Call(expr func, expr* args, keyword* keywords,
	#			 expr? starargs, expr? kwargs)
		super(GENERATE, self).generic_visit(node) 


	def visit_Expr(self, node):
		print "EXPR"
	#Expr(expr value)
		args = ""
		if (isinstance(node.value, _ast.Call)):
			for n in node.value.args:
				if (isinstance(n, _ast.Str)):
					args += n.s
				else:
					args += n.id	
		elif (isinstance(node.value, _ast.ClassDef)):
			self.data += "new "
		super(GENERATE, self).generic_visit(node) 
	
		self.data += ";;\n"


	def visit_arguments(self, node):
		print "arguments"
	#arguments = (expr* args, identifier? vararg, 
	#     			identifier? kwarg, expr* defaults)
		if (self.classflag):
			if (self.methodflag):
				return
		args = ""
		for a in node.args:
			args += ' ' + a.id + ' '
		if args == "":
			self.data += " () "
		args += " = "
		self.data += args
		

	def visit_FunctionDef(self, node):
		print "FUNCTION"
	#FunctionDef(identifier name, arguments args, 
	#				stmt* body, expr* decorator_list)
		rec = self.screen_recursive(node.name, node.body)
		name = node.name
		if (self.classflag == True):
			args = ""
			for n in node.args.args:
				if (n.id == "self"):
					continue
				else:
					args += n.id
			if (name)=='__init__':
				print "initializer"
				self.methodflag = True
				self.data += "initializer "
				super(GENERATE, self).generic_visit(node) 
			else:
				self.data += "method %s %s = " % (node.name, args)
				self.methodflag = True
			self.method_body(node.body)
			self.methodflag=False
		elif (rec): 
			self.data += "let rec %s " % name
			super(GENERATE, self).generic_visit(node) 
		else:
			self.data += "let %s " % name		
			super(GENERATE, self).generic_visit(node) 
		if (not self.classflag):
			self.data += ";;\n"
		#for b in node.body:
		#	super(GENERATE, self).generic_visit(b) 

		
	def visit_ClassDef(self, node):
		print "CLASS"
		self.classflag = True
	#ClassDef(identifier name, expr* bases, 
	#			stmt* body, expr* decorator_list)
		args = ""
		args = self.class_args(node.body)
		self.data += "class %s %s = object (self)\n" % (node.name, args)
		for b in node.bases:
			self.data += "inherit " + b.value + " \n"
		super(GENERATE, self).generic_visit(node) 
		self.data += "\nend;;\n"
		self.classflag = False
	

	def visit_Module(self, node):
		print "MODULE"
	#Module(stmt* body)
		super(GENERATE, self).generic_visit(node) 
		self.data += "\n;;\n"


	def visit_Assign(self, node):
		print "ASSIGNMENT"
	#Assign(expr* targets, expr value)
		to = ""
		fm = ""
		for t in node.targets:
			if (hasattr(t, 'id')):
				to += t.id + ' '
		if (isinstance(node.value, _ast.Num)):
			fm = node.value.n
		elif (isinstance(node.value, _ast.Str)):
			fm = '"' + node.value.s + '"'
		elif (isinstance(node.value, _ast.Call)):
			pass
		elif (isinstance(node.value, _ast.Name)):
			fm = node.value.id	

		else:
			pass

		if (self.classflag):
			if (self.methodflag):
				pass
			else:
				self.data += 'val mutable %s = %s' % (to, fm)
		else:
			self.data += """let %s = %s""" % (to, fm)

		if (isinstance(node.value, _ast.BinOp)):
			super(GENERATE, self).generic_visit(node.value) 
		elif (isinstance(node.value, _ast.Call)):
			super(GENERATE, self).generic_visit(node) 
		elif (isinstance(node.value, _ast.Name)):
			pass 
		elif (isinstance(node.value, _ast.Dict)):
			super(GENERATE, self).generic_visit(node) 
		elif (isinstance(node.value, _ast.Set)):
			super(GENERATE, self).generic_visit(node) 
		elif (isinstance(node.value, _ast.List)):
			super(GENERATE, self).generic_visit(node) 
		elif (isinstance(node.value, _ast.Tuple)):
			super(GENERATE, self).generic_visit(node) 
		elif (isinstance(node.value, _ast.Str)):
			pass
		elif (isinstance(node.value, _ast.Num)):
			pass
		elif (isinstance(node.value, _ast.Expr)):
			super(GENERATE, self).generic_visit(node) 
		else:
			print node.value
			pass

		if (not self.classflag):
			self.data += """;;\n"""
		else:
			self.data += """ """
	
	

	def visit_BoolOp(self,node):
		print "BOOLOP"
	#BoolOp(boolop op, expr* values)
		super(GENERATE, self).generic_visit(node) 


	def visit_BinOp(self, node):
		print "BINOP"
	#BinOp(expr left, operator op, expr right)
		super(GENERATE, self).generic_visit(node) 
		
	
	def visit_UnaryOp(self, node):
		print "UNARYOP"
	#UnaryOp(unaryop op, expr operand)
		super(GENERATE, self).generic_visit(node)
	
	def visit_Attribute(self, node):
		print "ATTRIBUTE"
	# Attribute(expr value, identifier attr, expr_context ctx)
		self.data += "%s#%s" % (node.value.id, node.attr)
		#super(GENERATE, self).generic_visit(node)



	def visit_For(self, node):
		print "FOR"
	#For(expr target, expr iter, stmt* body, stmt* orelse)

	def visit_While(self, node):
		print "WHILE"
	#While(expr test, stmt* body, stmt* orelse)

	def visit_If(self, node):
		print "IF"
	# If(expr test, stmt* body, stmt* orelse)

	def visit_With(self, node):
		print "WITH"
	# With(expr context_expr, expr? optional_vars, stmt* body)


	def visit_Dict(self,node):
		print "DICTIONARY"
	#Dict(expr* keys, expr* values)
		self.data += "{ "
		for k,v in zip(node.keys,node.values):
			if hasattr(k, 's'):
				self.data += '%s' % (k.s)
			elif hasattr(k, 'n'):
				self.data += '"%s"' % str(k.n)
			self.data += " = "
			if hasattr(v, 's'):
				self.data += '"%s"' % v.s
			elif hasattr(v, 'n'):
				self.data += '"%s"' % str(v.n)
			self.data += ";"
		self.data += " }"

	def visit_Set(self,node):
		print "SET"
	#Set(expr* elts)
		self.data += "Set.of_list [	"
		for e in node.elts:
			self.data += '"%s";' % (e.s)
		self.data += "]"

	def visit_List(self,node):
		print "LIST"
	#List(expr* elts, expr_context ctx) 
		self.data += "["
		for e in node.elts:
			self.data += '"%s";' % (e.s)
		self.data += "]"

	def visit_Tuple(self, node):
		print "TUPLE"
	#Tuple(expr* elts, expr_context ctx)
		self.data += "( "
		for e in node.elts:
			if e == node.elts[-1]:
				self.data += '"%s"' % e.s
			else:
				self.data += '"%s",' % e.s
		self.data += " )"

